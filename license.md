/*  
 * ------------------------------------------------------------  
 * "THE BEERWARE LICENSE" (Revision 42):  
 * Jef Mertens wrote this documentation. As long as you retain the link    
 * to this notice, you can do whatever you want with this stuff. If we  
 * meet someday, and you think this stuff is worth it, you can buy me   
 * a beer in return.  
 * ------------------------------------------------------------  
*/  